const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose');
const mongodb = require('mongodb');
const validator = require('express-validator');
const { celebrate, Joi, errors } = require('celebrate');
const DB = require("./server/conn/conn");
const uplink = require('./server/model/upload');
const multer = require('multer');
const jwt = require('jsonwebtoken');
const secret = "SomeSecretHere";
const auth  = require('./server/routes/auth');
const unique = require('./server/routes/unique');
const student = require('./server/model/students');
const del = require('./server/routes/del');
const ajay = require('./server/project/ajay/ajay');

const port = 3000;

const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use('/auth', auth);
app.use('/unique', unique);
app.use('/del', del);
app.use('/ajay', ajay);
app.use(express.static('upload'))



app.get('/', function(req, res){
    res.send("Welcome")
})

app.post('/', function(req, res){
    res.send('WELCOME')
}
)


var store = multer.diskStorage({
    destination: function(req, file, cb){
      cb(null, './upload')
    },
    filename: function(req, file, cb){
      console.log(file)
      cb(null, file.originalname)
    }
  })
 
  var upload = multer({
    storage: store,
    fileFilter: function(req, file, cb){
      var ext = path.extname(file.originalname);

      var query = uplink.findOne({ pdfname: file.originalname});

      if( ext != '.pdf'){
        return cb(new Error("please provide pdf file only"), null)
        // cb(null, false);
      }else{
        query.exec(function(err, result){
          if(err){

             return cb(new Eror(err), null, false)
          }else if(result){

            return cb(new Error("File Already Exist"), null, false)
          }else{

            cb(null, true);
          }
        })
        // cb(null, true);
      }
    }
  }).single('userFile');

  const getdu = celebrate({
    body: Joi.object({
      pdname: Joi.string().required(),
      sub: Joi.string().required(),
      sem: Joi.string().required(),
      uploader: Joi.string().required()
    })
  })

app.post('/upload', function(req, res){

 upload(req, res, function(err){
   if(err){
      res.send(err+"")
   }else{

    var pdname = req.body.pdname;
    var sub = req.body.sub;
    var subid = req.body.subid;
    var chapter = req.body.chapter;
    var sem = req.body.sem;
    var uploadby = req.body.uploader;
    var course = req.body.course;
    var branch = req.body.branch;
    var year = req.body.year;
    // console.log(req.body)

    const Uplink = new uplink({
      pdfname: req.file.originalname,
      name: pdname,
      sub: sub,
      subid: subid,
      chapter: chapter,
      sem: sem,
      uploader: uploadby,
      course: course,
      branch: branch,
      year: year,
      url: "http://139.59.26.186/"+req.file.originalname
    })

    Uplink.save()
    .then(docs =>{
    
    res.send(" File Uploaded successfully")
    })
    .catch(err =>{
    console.log(err)
    res.send("There is an Error ")
    })
   }

 })  
          
})
app.use((err, req, res, next) =>{
    // res.send("Validation Error")
    console.log(err)
   });

// app.get('/list', function(req, res){
//    var sem = req.body.sem;
//         uplink.find({})
//               .exec()
//               .then(docs =>{
                
//                 const response ={
//                   count: docs.length,
//                   PdfList: docs.map(doc =>{
//                     return{
//                       name: doc.name,
//                       pdfname: doc.pdfname,
//                       sub: doc.sub,
//                       sem: doc.sem,
//                       uploader: doc.uploader,
//                       url: doc.url
//                     }
//                   })
//                 }
//                  res.send(response)
//               })
//               .catch(err =>{
//                 res.send(err)
//               })
    
// })


app.post('/list', function(req, res){

  var sem = req.body.sem;
  var subid = req.body.subid;
  var chapter = req.body.chapter;
  var token = req.body.token;
  
  if(token){
    jwt.verify(token, secret, function(err, decode){
      if(err){
        res.send(err)
      }else{
        req.decode = decode;
        uplink.find({ $and: [{sem: sem},{ subid: subid},{chapter: chapter}]})
             .exec()
             .then(docs =>{
               
               const response ={
                 count: docs.length,
                 PdfList: docs.map(doc =>{
                   return{
                    id: doc._id,
                    name: doc.name,
                    pdfname: doc.pdfname,
                    sub: doc.sub,
                    sem: doc.sem,
                    uploader: doc.uploader,
                    url: doc.url,
                    course: doc.course,
                    branch: doc.branch,
                    year: doc.year
                   }
                 })
               }
                res.send(response)
             })
             .catch(err =>{
               res.send(err)
             })
      }
    })
  }else{
    res.send('Auth Error!')
  }

      
   
})

app.get("/scount", function(req, res){

  DB.collection('students').find().toArray(function(err, result){
    if(err){

      res.send("error")
    }else{
      res.send(""+result.length)
    }
  })

})



app.get("/tcount", function(req, res){

  DB.collection('teacher').find().toArray(function(err, result){
    if(err){

      res.send("error")
    }else{
      res.send(""+result.length)
    }
  })
  
})



app.get("/subcount", function(req, res){

  DB.collection('subjects').find().toArray(function(err, result){
    if(err){

      res.send("error")
    }else{
      res.send(""+result.length)
    }
  })
  
})

app.get("/pdfcount", function(req, res){

  DB.collection('uploadlinks').find().toArray(function(err, result){
    if(err){

      res.send("error")
    }else{
      res.send(""+result.length)
    }
  })
  
})

app.post('/sub', function(req, res){
  var course = req.body.course;
  var branch = req.body.branch;
  var sem = req.body.sem;
   
  DB.collection('subjects').find({ 
    $and: [
           { course: course },{ branch: branch },{ sem: sem}
          ]
    })
    .toArray(function(err, result){
    if(err){
      res.json({success: false,
        Error: err})
    }else if(!result){
      res.json({success: false,
               Error: "No Subject Found"})
    }else{

      const respo = {
        success: true,
        subjects: result.map(list =>{
          return{
            name: list.name,
            branch: list.branch,
            id: list._id
          }
        })
      }

      res.send(respo);
     
    }
  })
  
 })

app.get('/course', function(req, res){

  DB.collection('course').find().toArray(function(err, courses){
    if(err){
      res.json({
        success: false,
        Error: err
      })
    }else{

      const respo = {
        success: true,
        Name: courses.map(cs =>{
         return{
           name: cs.cousre,
           year: cs.toyear,
           id: cs._id
         }
        })
      }
      res.send(respo)
      
    }
  })
})

app.get('/ccourse', function(req, res){

  var course = req.body.course;


  DB.collection('course').find({cousre: course }).toArray(function(err, courses){
    if(err){
     res.send(err)
    }else{

     res.send(courses)
      
    }
  })
})

app.post('/branch', function(req, res){
  var course = req.body.course;

  DB.collection('course').find({cousre: course}).toArray(function(err, branches){
    if(err){
      res.json({
        success: false,
        Error: err
      })
    }else{

    

     res.json({
       success: true,
       cbranch: branches
     })
    }
  })

})
app.post('/sub', function(req, res){
  var course = req.body.course;
  var branch = req.body.branch;
  var sem = req.body.sem;
   
  DB.collection('subjects').find({ 
    $and: [
           { course: course },{ branch: branch },{sem: sem} 
          ]
    })
    .toArray(function(err, result){
    if(err){
      res.send(err)
    }else if(!result){
      res.send('not found')
    }else{

      const respo = {
        subjects: result.map(list =>{
          return{
            name: list.name
          }
        })
      }

      res.send(respo);
     
    }
  })
  
 })

app.post('/students', function(req, res){

 var aid = req.body.aid;
 var name = req.body.sname;
 var no  = req.body.no;
 var course = req.body.course;
 var branch = req.body.branch;
 var year = req.body.year;
 var sem = req.body.sem;

 const Students = new student({
  aid: aid,
  name: name,
  no: no,
  course: course,
  branch: branch,
  year: year,
  cid: aid,
  sem: sem,
  blocked: false
})

 Students.save()
     .then(std =>{
          res.send("success")
         })
     .catch(er =>{
          res.send(er)
         })


})

app.post('/updatestudents', function(req, res){

  var aid = req.body.aid;
  var name = req.body.sname;
  var no  = req.body.no;
  var course = req.body.course;
  var branch = req.body.branch;
  var year = req.body.year;
 
  const Students = new student({
   name: name,
   course: course,
   branch: branch,
   year: year
 })
 
  student.findOne({ aid: aid})
         .exec()
         .then(docs =>{
           if(docs){
 
             DB.collection('students').updateOne({aid: aid},{
             
               $set: {
                 name: name,
                 course: course,
                 branch: branch,
                 year: year
                }
             }, function(err, result){
               if(err){
                 res.send(err)
               }else{
                 res.send('success')
               }
             })
 
           }
         })
         .catch(err =>{
           res.send(err)
         })
  
 
 })

app.post('/address', function(req, res){

  var line1 = req.body.line1;
  var line2 = req.body.line2;
  var line3 = req.body.line3;
  var landline = req.body.landline;
  var mo = req.body.mo;
  var id = req.body.cid;
  var pin = req.body.pincode;
  var about = req.body.about;
  var where = req.body.where;
  var title = req.body.title;
  // DB.collection('address').insert( {line1: line1, line2:line2, line3:line3, landline:landline,mobile:mo}, function(err, result){
  //   if(err){
  //     res.send(err)
  //   }else{
  //     res.json({
  //               success:true
  //     })
  //   }
  // })

  // DB.collection('address').insert( {line1: line1, line2:line2, line3:line3,pincode: pin ,landline:landline,mobile:mo, id: "address"}, function(err, result){
  //   if(err){
  //     res.send(err)
  //   }else{
  //     res.json({
  //       id:"address",
  //       success:true
  //     })
  //   }
  // });

   if( id == "" || id == null || where == "" || where == null){
     res.send('Emty Value')
   }else if(where == "about"){

    DB.collection('address').updateOne({ id: id}, { $set: {title:title, about: about} }, function(err, result){
      if(err){
        res.send(err)
      }else{
        res.json({
          id:result,
          success:true
        })
      }
    })
   }else if(where == "address"){

  DB.collection('address').updateOne({ id: id}, { $set: {line1: line1, line2:line2, line3:line3,pincode:pin, landline:landline,mobile:mo} }, function(err, result){
       if(err){
         res.send(err)
       }else{
         res.json({
           id:result,
           success:true
         })
       }
     })

   }else{
     res.send('something wrong')
   }

   
    

  

})

app.get('/address', function(req, res){

  DB.collection('address').find().toArray(function(err, result){
    if(err){
      res.send(err)
    }else if(!result){
      res.send('NO Address Fond.')
    }else{
      res.send(result)
    }
  })

})


app.get('*',(req,res) =>{
    res.send("404 Not Found");
    });


app.post('*',(req,res) =>{
      res.send("404 Not Found");
      });    
    
app.listen(process.env.PORT || port,function(){
      console.log("Server runing on " + port);
    })