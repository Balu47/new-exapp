const mongoose = require('mongoose');
const { celebrate, Joi, errors } = require('celebrate');
const DB = require("../conn/conn");
const oid = require('mongodb').ObjectID;
const uplink = require('../model/upload');
const jwt = require('jsonwebtoken');
const secret = "SomeSecretHere";
const express = require('express');
const router = express.Router();


const teacherD = celebrate({
    body: Joi.object({
      tname: Joi.string().required(),
      pass: Joi.string().required(),
      sub: Joi.string().required()
    }),
    headers: Joi.object({
      token: Joi.string().required()
    }).unknown()
  })
  
  const AdminD = celebrate({
    body: Joi.object({
      Aname: Joi.string().required(),
      Apass: Joi.string().required()
    })
  })
  
  const checkT = celebrate({
    headers: Joi.object({
      token: Joi.string().required()
    }).unknown()
  })


  
  router.post('/teacherlog', function(req, res){
    var name = req.body.tname;
    var pass = req.body.pass;
  
    DB.collection('teacher').findOne({ email: name, pwd: pass}, function(err, foundT){
      if(err){
        res.send(err)
      }else if(!foundT){
        res.send("fail")
      }else{
        res.send('success')
      }
    })
  })
  
  router.post('/addteacher', function(req, res){
  
    var tname = req.body.tname;
    var pass = req.body.pass;
    var sub = req.body.sub;
    var no = req.body.no;
    var email = req.body.email;
    var course = req.body.course;
    var branch = req.body.branch;
    var year = req.body.year;

    var token = req.headers.token;
    // var token = req.body.token || req.query.token || req.header['x-access-token'];
  
    if(token){
      jwt.verify(token, secret, function(err, decode){
        if(err){
          res.send(err)
          
        }else{
          req.decode = decode;
          DB.collection('teacher').insert({
             name: tname,
             pwd: pass,
             sub: sub,
             no: no,
             email: email,
             course: course,
             branch: branch,
             year: year
            }, function(err, tea){
                    if(err){
                    res.send(err)
                    }else{
                    res.json({
                      success: true,
                      name: tname,
                      pwd: pass,
                      sub: sub,
                      no: no,
                      email: email,
                      course: course,
                      branch: branch
                    })
                    }
    })
      }
      })
    }else{
      res.json({
        success: false,
        message: 'NO token providede'
      })
    }
  
    
  })
  

  router.post('/allist', function(req, res){
    var year = req.body.year;
    var subid = req.body.subid;
    var chapter = req.body.chapter;
        
    uplink.find({ $and: [{year: year},{ subid: subid},{chapter: chapter}]})
             .exec()
             .then(docs =>{
               
               const response ={
                 count: docs.length,
                 PdfList: docs.map(doc =>{
                   return{
                     id: doc._id,
                     name: doc.name,
                     pdfname: doc.pdfname,
                     sub: doc.sub,
                     sem: doc.sem,
                     uploader: doc.uploader,
                     url: doc.url,
                     course: doc.course,
                     branch: doc.branch,
                     year: doc.year
                   }
                 })
               }
                res.send(response)
             })
             .catch(err =>{
               res.send(err)
             })
  
     
  })
  const CoURse = celebrate({
    body: Joi.object({
      course: Joi.string().required(),
      nofse: Joi.string().required(),
      toyea: Joi.string().required()
    }),
    headers: Joi.object({
      token: Joi.string().required()
    }).unknown()
  })
  router.post('/course', function(req, res){
    var token = req.headers.token;
    var course = req.body.cbname;
    var nofse = req.body.nofsem;
    var toyea = req.body.toyear;


    

    if(token){
      jwt.verify(token, secret, function(err, decode){
        if(err){
          res.send(err)
        }else{
          req.decode = decode;
          DB.collection('course').insert({
            cousre: course,
            nofsem: nofse,
            toyear: toyea,
            branches: []
      
          }, function(err, resu){
            if (err){
             res.send(err)
            }else{
             res.send("success")
            }
          })
        }
      })
    }else{
      res.json({
        success: false,
        message: 'NO token providede'
      })
    }


  })

  router.post('/branches', function(req, res){
    var course = req.body.course;
    var branch = req.body.cbname;
    var token = req.headers.token;
  
    if(token){
      jwt.verify(token, secret, function(err, decode){
        if(err){
          res.send(err)
        }else{
          req.decode = decode;
          DB.collection('course').update({cousre: course},{$addToSet: {branches: {name: branch}}}, function(er, result){
            if(er){
              res.send(er)
            }else{
              res.send('success')
            }
          })
        }
      })
    }else{
      res.json({
        success: false,
        message: 'NO token providede'
      })
    }



  })

  const SUBJECTS = celebrate({
    body: Joi.object({
       sname: Joi.string().required(),
      course: Joi.string().required(),
      branch: Joi.string().required(),
      year: Joi.string().required(),
      sem: Joi.string().required()
    })
  })
  
  router.post('/addsub',SUBJECTS, function(req, res){
    var name = req.body.sname;
    var course = req.body.course;
    var branch = req.body.branch;
    var year = req.body.year;
    var sem = req.body.sem;
    
    DB.collection('subjects').insert({
      name: name,
      course: course,
      branch: branch,
      year: year,
      sem: sem,
     chapters: []
    }, function(err, result){
       if(err){ res.send(err)}
       else{ res.send('success')}
    })


  })

 router.post('/sub', function(req, res){
  var course = req.body.course;
  var branch = req.body.branch;
  var year = req.body.year;
   
  DB.collection('subjects').find({ 
    $and: [
           { course: course },{ branch: branch },{ year: year}
          ]
    })
    .toArray(function(err, result){
    if(err){
      res.json({success: false,
        Error: err})
    }else if(!result){
      res.json({success: false,
               Error: "No Subject Found"})
    }else{

      const respo = {
        success: true,
        subjects: result.map(list =>{
          return{
            name: list.name,
            branch: list.branch,
            id: list._id
          }
        })
      }

      res.send(respo);
     
    }
  })
  
 })

 router.post('/addch', function(req, res){
  var id = req.body.sid;
  var chapter = req.body.chapter;

  DB.collection('subjects').updateOne({ '_id': new oid(id)},{$addToSet: {chapters: {name: chapter}}}, function(er, result){
    if(er){
      res.send(er)
    }else{
      res.send('success')
    }
  })
 })

 router.post('/cha', function(req, res){

 var sid = req.body.sid;


  DB.collection('subjects').findOne({'_id': new oid(sid)},function(err, result){
    if(err){ res.json({
      success: false,
      Error: err
    })}
    else{
      // res.send(result.chapters)

      const respo = {
        success: true,
        id: result._id,
        chapters: result.chapters
      }

      res.send(respo);
    }
  })
 })

 router.post('/getSinfo', function(req, res){

  var id = req.body.sid;

  DB.collection('students').findOne({aid: id}, function(err, result){
    if(err){

      res.json({
        success:false
      })
    }else{

  res.json({
    success: true,
    result: result
  })

    
    }

  })

})

  router.post('/students', function(req, res){

    var token = req.body.token;    
    var branch = req.body.branch;
    var year = req.body.year;


    if(token){
      jwt.verify(token, secret, function(err, decode){
        if(err){
          res.send(err)
        }else{
          req.decode = decode;
          DB.collection('students').find({ $and:[{ branch: branch}, {year: year}]}).toArray(function(err, students){
            if(err){
              res.send(err)
            }else{
              if(!students){
                res.send('no student found')
              }else{
               
               res.json({
                count: students.length,
                Students:students
               })
              }
            }
          })
        }
      })
    }else{
     res.send('Auth Error!')
    }



   
  })

 

  router.post('/teachers', function(req, res){

    var token = req.body.token;
    var branch = req.body.branch;
    var year = req.body.year; 

    if(token){
      jwt.verify(token, secret, function(err, decode){
        if(err){
          res.send(err)
        }else{
          req.decode = decode;
          DB.collection('teacher').find({branch: branch}).toArray(function(err, teacheres){

            if(err){
              res.send(err)
            }else{
              res.json({
                count: teacheres.length,
                TeacherList: teacheres
              })
            }
          })
        }
      })
    }else{
      res.send('Auth Error!')
    }


   
  })

  router.post('/list', function(req, res){
  
    var token = req.body.token;
    // var token = req.body.token || req.query.token || req.header['x-access-token'];
  
    if(token){
      jwt.verify(token, secret, function(err, decode){
        if(err){
          res.send(err)
        }else{
          req.decode = decode;
          DB.collection('teacher').find().toArray(function(err, tlist){
            if(err){
            
              res.send(err)
            }else{
              const respo = {
                count: tlist.length,
                TeacherList: tlist.map(tlis =>{
                 return{
                   name: tlis.name,
                   subject: tlis.sub
                 }
                })
              }
              res.send(respo)
            }
          })
        }
      })
    }else{
      res.json({
        success: false,
        message: 'NO token providede'
      })
    }
  
   
  })

  router.use((err, req, res, next) =>{
    res.status(400).json(err);
  });

router.post('/admin',AdminD, function(req, res){
    var name = req.body.Aname;
    var pass = req.body.Apass;
  
    DB.collection('admin').findOne({ name: name, pwd: pass}, function(err, admin){
      if(err){
        res.send(err)
      }else if(admin){
        const payload = { admin: admin.name};
        var token = jwt.sign(payload, secret);
  
        res.json({
          success: true,
          message: 'Here is your token',
          token: token
        });
      }else{
          res.send("Auth Error")
      }
    })
  })



module.exports = router;