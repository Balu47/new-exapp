const mongoose = require('mongoose');
const { celebrate, Joi, errors } = require('celebrate');
const DB = require("../conn/conn");
const mongodb = require('mongodb');
const oid = require('mongodb').ObjectID;
const uplink = require('../model/upload');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const secret = "SomeSecretHere";
const express = require('express');
const router = express.Router();


const CID = celebrate({
    body: Joi.object({
      cid: Joi.string().required()
     
    }),
    headers: Joi.object({
      token: Joi.string().required()
    }).unknown()
  })
  
  // const CID = celebrate({
  //   body: Joi.object({
  //     cid: Joi.string().required(),
  //     token: Joi.string().required()
  //   })
  // })
  
  const Branch = celebrate({
    body: Joi.object({
      bid: Joi.string().required(),
      bname: Joi.string().required()
    }),
    headers: Joi.object({
      token: Joi.string().required()
    }).unknown()
  })

router.post('/teacher', CID, function(req, res){

    var tid = req.body.cid;
    var token = req.headers.token;

    if(token){
        jwt.verify(token, secret, function(err, decode){
          if(err){
            res.send(err)
          }else{
            req.decode = decode;
            DB.collection('teacher').deleteOne({ _id: new mongodb.ObjectID(tid)}, function(err, obj){
                if(err){
                    res.send(err)
                }else{
                    res.send('Deleted.'+obj)
                }
            })
          }
        })
      }else{
        res.send('Auth Error!')
      }

    
})

router.post('/course',  function(req, res){

    var cid = req.body.cid;
    var token = req.headers.token;
    var course = req.body.course;

    if(token){
        jwt.verify(token, secret, function(err, decode){
          if(err){
            res.send(err)
          }else{
            req.decode = decode;
           DB.collection('uploadlinks').find({course: course}).toArray(function(err, result){
             if(err){
               res.send(err)
             }else if(result.length === 0){

              DB.collection('course').deleteOne({ $and: [ { '_id': new oid(cid)}, {cousre: course} ]}, function(err, result){
                if(err){
                    res.send(err)
                }else{
                   if(result.n === 1){
                     res.send('Deleted.')
                   }else{
                     res.send(' Deleted')
                   }
                }
            })
             }else{

              var len = result.length;
              res.send("there is "+len+" pdf found in this course delete it and try again.")

             }
           })
          }
        })
      }else{
        res.send('Auth Error!')
      }
})

router.post('/branch',Branch, function(req, res){
    var bid = req.body.bid;
    var name = req.body.bname;
    var token = req.headers.token;

    if(token){
        jwt.verify(token, secret, function(err, decode){
          if(err){
            res.send(err)
          }else{
            req.decode = decode;
            DB.collection('uploadlinks').find({branch: name}).toArray(function(err, result){
              if(err){
                res.send(err)
              }else if(result.length === 0){

                DB.collection('course').updateOne({ '_id': new oid(bid)},{ $pull: { branches: { name: name}}}, function(err, result){
                  if(err){
                       res.send(err)
                         }else{
                         if(result.modifiedCount >= 1){
                           res.send('Deleted.')
                         }else{
                           res.send('Deleted')
                         }
                       }
                   }) 

              }else{
               
                res.send("there is "+result.length+" pdf found in this branch delete it  and try again.")

              }
            })
         
          }
        })
      }else{
        res.send('Auth Error!')
      }


})

const Subjects = celebrate({
  body: Joi.object({
    sid: Joi.string().required()
  })
})

router.post('/sub',Subjects, function(req, res){
var id = req.body.sid;

  DB.collection('uploadlinks').find({ subid: id}).toArray(function(err, result){
    if(err){
      res.send(err)
    }else if(result <= 0){

     DB.collection('subjects').deleteOne({ _id: new oid(id)}, function(err, results){
       if(err){
           res.send(err)
       }else{
          if(results.n === 1){
            res.send('Deleted.'+results)
          }else{
            res.send(' Deleted '+results.n)
          }
       }
   })
    }else{

     var len = result.length+1;
     res.send("there is "+len+" pdf found in this subject delete it and try again.")

    }
  })
})

const Chapter = celebrate({
  body: Joi.object({
    sid: Joi.string().required().trim(),
    cname: Joi.string().required()
  })
})

router.post('/chap',Chapter, function(req, res){
var id = req.body.sid;
var name = req.body.cname;

  DB.collection('uploadlinks').find({$and:[{chapter: name},{subid: id}]}).toArray(function(err, result){
    if(err){
      res.send(err)
    }else if(result == 0){

      DB.collection('subjects').updateOne({ '_id': new oid(id)},{ $pull: { chapters: { name: name}}}, function(err, result){
        if(err){
             res.send(err)
               }else{
               if(result.modifiedCount >= 1){
                 res.send('Deleted.')
               }else{
                 res.send(' Deleted')
               }
             }
         }) 

    }else{
     
      res.send("there is "+result.length+" pdf found in this chapter delete it  and try again.")

    }
  })


 
})

router.post('/bstudent', CID, function(req, res){
    var sid = req.body.cid;
    var token = req.headers.token;

    if(token){
        jwt.verify(token, secret, function(err, decode){
          if(err){
            res.send(err)
          }else{
         
    DB.collection('students').update({ aid: sid},{ $set: { blocked: true}}, function(err, result){
        if(err){
            res.send(err)
        }else{
            res.send("Student Blocked. "+result)
        }
      })  
          }
        })
      }else{
        res.send('Auth Error!')
      }
    


})

router.post('/ubstudent', CID, function(req, res){
    var sid = req.body.cid;
    var token = req.headers.token;

    if(token){
        jwt.verify(token, secret, function(err, decode){
          if(err){
            res.send(err)
          }else{
            req.decode = decode;
            DB.collection('students').update({ aid: sid},{ $set: { blocked: false}}, function(err, result){
                if(err){
                    res.send(err)
                }else{
                    res.send("Student Unblocked. "+result)
                }
            })
          }
        })
      }else{
        res.send('Auth Error!')
      }

    

})

router.post('/checkBlock', CID, function(req, res){
  var sid = req.body.cid;
  var token = req.headers.token;

  if(token){
      jwt.verify(token, secret, function(err, decode){
        if(err){
          res.send(err)
        }else{
          req.decode = decode;
          DB.collection('students').findOne({ aid: sid}, function(err, result){
              if(err){
                  res.send(err)
              }else{
                if(!result){
                  res.send("error")
                }else if(result.blocked == true){
                  res.send("true")
                }else{
                  res.send("false")
                }
              }
          })
        }
      })
    }else{
      res.send('Auth Error!')
    }

  

})

router.post('/delete', function(req, res){

  var token = req.body.token;
  var name = req.body.pdfname;

  
  if(token){
    jwt.verify(token, secret, function(err, decode){
      if(err){
        res.send(err)
      }else{
        req.decode = decode;
        fs.unlink('upload/'+name, function(err){
          if(err){
            res.send( 'File Not Delete ')
          }else{
            DB.collection('uploadlinks').deleteOne({ pdfname: name}, function(er, re){
              if(er){
                res.send('Database Error '+er)
              }else if(re){
                res.send('No Error '+re.result.n)
              }else{
                res.send( 'No File Found in Database')
              }
            })
          }
        })
       
      }
    })
  }else{
    res.send('Auth Error!')
  }
})

router.use((err, req, res, next) =>{
    res.status(400).json(err);
  });
module.exports = router;
