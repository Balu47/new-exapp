const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const uplinkschema = new Schema({
    pdfname: { type:String, required:true},
    name: { type:String, required:true },
    sub: { type:String, required:true},
    subid: { type:String, required:true},
    chapter: { type:String, required: true},
    sem: { type:String, required:true},
    course: { type:String, required:true},
    branch: { type:String, required:true},
    year: { type:String, required:true},
    uploader: { type:String, required:true},
    url: { type:String, required:true}
});

var uplink = mongoose.model('uploadlinks', uplinkschema)
module.exports = uplink

