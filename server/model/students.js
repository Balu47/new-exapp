const mongoose = require('mongoose')

const Schema = mongoose.Schema;

const studentss = new Schema({

    aid: { type:String, required: true},
    name: { type:String, required:true},
    no: { type:String, required:true },
    course: { type:String, required:true},
    branch: { type:String, required:true},
    year: { type:String, required:true},
    sem : { type:String, required:true},
    blocked: {default: false}

});

var students = mongoose.model('students', studentss)
module.exports = students