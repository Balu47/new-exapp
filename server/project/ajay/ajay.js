
const nodemailer = require('nodemailer');
const express = require('express');
const router = express.Router();

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: 'techtalk.india45@gmail.com',
      pass: 'techtalk.myindia'
    },
    tls: {
      rejectUnauthorized: false
  }
  })

  router.post('/send', function(req, res){
      var name = req.body.sname;
      var pid = req.body.pid;
      var addr = req.body.addr;

      const mailOptios = {
        from : 'Pratik Digital World',
        to: "akarahale@gmail.com",
        subject: 'Customer Buy Product',
        html: '<body><div style=\"background-color:orange;padding:10px;color:black;border-style:solid;border-color:black;\"> <h3> Customer Name : '+name+'</h3> </br> <h3> Product ID : '+pid+'</h3></br><h3> Customer Address :  '+addr+' </h3></div><body>'
        };
  
      transporter.sendMail(mailOptios, function(err, info){
        if(err){
          res.send('Failed to Send '+err)
        }else{
          res.send('success')
        }
      })
  })

  router.post('/feedback', function(req, res){
    var name = req.body.sname;
    var feed = req.body.feed;

    const mailOptios = {
      from : 'Pratik Digital World',
      to: "akarahale@gmail.com",
      subject: 'Customer Feedback',
      html: '<body> <div style=\"background-color:orange;padding:10px;color:black;border-style:solid;border-color:black;\"> <h3> Customer Name : '+name+'</h3><h3> Customer Feedback : '+feed+' </h3> </div> </body>'
      };

    transporter.sendMail(mailOptios, function(err, info){
      if(err){
        res.send('Failed to Send '+err)
      }else{
        res.send('success')
      }
    })
})

router.use((err, req, res, next) =>{
    res.status(400).json('Bad request');
  });

  module.exports = router ;